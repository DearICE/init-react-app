/*
 * 入口文件
 * @Author: 卢冰豪
 * @Date: 2019-01-31 15:42:20
 * @Last Modified by: 卢冰豪
 * @Last Modified time: 2019-06-28 19:14:59
 */

import React from 'react';
import ReactDOM from 'react-dom';
import 'amfe-flexible';
import FastClick from 'lib/fastclick';
import './lib/native$';
import './app.scss';
import Main from './Main';

if ('addEventListener' in document && /iP(hone|od|ad)/.test(navigator.appVersion)) {
  document.addEventListener('DOMContentLoaded', () => {
    FastClick.attach(document.body);
  }, false);
}

ReactDOM.render(
  <Main />,
  document.getElementById('root'),
);
